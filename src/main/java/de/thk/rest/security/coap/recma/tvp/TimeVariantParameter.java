package de.thk.rest.security.coap.recma.tvp;

public interface TimeVariantParameter {
	public int generate();
	public boolean verify(int tvp);
}
