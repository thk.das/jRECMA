package de.thk.rest.security.coap.recma;

import java.net.URI;
import java.net.URISyntaxException;

import ch.ethz.inf.vs.californium.coap.Request;
import de.uzl.itm.ncoap.message.CoapRequest;
import de.uzl.itm.ncoap.message.MessageCode;
import de.uzl.itm.ncoap.message.MessageType;


public class App 
{
    public static void main( String[] args ) throws IllegalArgumentException, URISyntaxException
    {
    	Request request = Request.newGet();
    	CoapRequest coapRequest = new CoapRequest(MessageType.NON, MessageCode.GET, new URI("coap://test"));
    	int version = coapRequest.getProtocolVersion();
    	int type = coapRequest.getMessageType();
    	int tokenLength = coapRequest.getToken().getBytes().length;
    	int code = coapRequest.getMessageCode();
    }
}
