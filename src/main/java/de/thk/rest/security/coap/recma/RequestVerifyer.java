package de.thk.rest.security.coap.recma;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import de.thk.rest.security.coap.recma.hash.BodyHasher;
import de.thk.rest.security.coap.recma.sig.TbsAuthenticator;
import de.uzl.itm.ncoap.message.CoapRequest;
import de.uzl.itm.ncoap.message.options.OptionValue;

public class RequestVerifyer extends MessageVerifyer{
	
	public RequestVerifyer(TbsAuthenticator tbsAuthenticator, BodyHasher bodyHasher) {
		super(tbsAuthenticator, bodyHasher);
		// TODO Auto-generated constructor stub
	}

	public void verify(CoapRequest req){
		
		 int sign = byteArrayToInt(req.getOptions(65021).iterator().next().getValue());
		 int hash = byteArrayToInt(req.getOptions(65053).iterator().next().getValue());
		 int kid = byteArrayToInt(req.getOptions(65181).iterator().next().getValue());
		 int sv = byteArrayToInt(req.getOptions(65117).iterator().next().getValue());
		 int tvp = byteArrayToInt(req.getOptions(65085).iterator().next().getValue());
		 
		 //desc
		 ArrayList<Short> addOptions = new ArrayList<>();
		 
		 Set<OptionValue> addOptionsSet = req.getOptions(65149);
		 
		 for(Iterator<OptionValue> iterator = addOptionsSet.iterator(); iterator.hasNext();){
			 OptionValue optionValue = iterator.next();
			 addOptions.add(byteArrayToShort(optionValue.getValue()));
		 }
		 
		 
	}

}
