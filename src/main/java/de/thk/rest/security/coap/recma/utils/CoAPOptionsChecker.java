package de.thk.rest.security.coap.recma.utils;

public class CoAPOptionsChecker {
	public static void main(String[] args) {
//		check(65000);
		System.out.println(findSignatureOptionNumber(65151));
	}
	
	public static int findSignatureOptionNumber(int startnumber){
		
		boolean c = false;
		boolean s = false;
		boolean nck = false;
		while(true){
			c = isCritical(startnumber);
			s = !isUnSafe(startnumber);
			nck = isNoCacheKey(startnumber);
			if(c && s && nck){
				return startnumber;
			} else {
				c = false;
				s = false;
				nck = false;
				startnumber++;
			}
		}
	}
	
	public static void check(int number){
		if(isCritical(number)){
			System.out.println("Critical");
		} else {
			System.out.println("Elective");
		}
		
		if(isUnSafe(number)){
			System.out.println("Unsafe");
		} else {
			System.out.println("Safe");
		}
		
		if(isNoCacheKey(number)){
			System.out.println("NoCacheKey");
		} else {
			System.out.println("CacheKey");
		}
		
	}
	
	public static boolean isCritical(int number){
		return (number & 1) == 1 ? true : false;
	}
	
	public static boolean isUnSafe(int number){
		return (number & 2) == 1 ? true : false;
	}
	
	public static boolean isNoCacheKey(int number){
		return (number & 0x1e) == 0x1c ? true : false;
	}
}
