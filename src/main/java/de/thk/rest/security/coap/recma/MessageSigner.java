package de.thk.rest.security.coap.recma;

import java.util.ArrayList;

import de.thk.rest.security.coap.recma.hash.BodyHasher;
import de.thk.rest.security.coap.recma.sig.TbsAuthenticator;
import de.uzl.itm.ncoap.message.CoapMessage;
import de.uzl.itm.ncoap.message.options.OpaqueOptionValue;

public class MessageSigner extends MessageAuthentication {
	
	public MessageSigner() {
		super();
	}
	
	public MessageSigner(TbsAuthenticator tbsAuthenticator, BodyHasher bodyHasher){
		this.getTbsAuthenticatorsHashMap().put(tbsAuthenticator.getNumber(), tbsAuthenticator);
		this.getBodyHashersHashMap().put(bodyHasher.getNumber(), bodyHasher);
	}
	
	public void sign(CoapMessage msg, int kid, int sig, int hash, ArrayList<Short> addOptions) throws Exception{
		int tvp = getTimeVariantParameter().generate();
		byte[] tbs = buildTbs(msg, tvp, hash, addOptions);
		TbsAuthenticator tbsAuthenticator = getTbsAuthenticatorsHashMap().get(sig);
		byte[] sigValue = tbsAuthenticator.sign(kid, tbs);
		//Signature-Algorithm 
		msg.getAllOptions().put(new Integer(65021), new OpaqueOptionValue(65021, intToByteArray( sig)));
		//Hash-Algorithm
		msg.getAllOptions().put(new Integer(65053), new OpaqueOptionValue(65053, intToByteArray( hash)));
		//TVP
		msg.getAllOptions().put(new Integer(65085), new OpaqueOptionValue(65085, intToByteArray(tvp)));
		//Signature-Value
		msg.getAllOptions().put(new Integer(65117), new OpaqueOptionValue(65117, sigValue));
		//Additional-Options
		for(Short addOption : addOptions){
			msg.getAllOptions().get(65149).add(new OpaqueOptionValue(new Integer(65149), intToByteArray(addOption)));
		}
		//Key-ID
		msg.getAllOptions().put(new Integer(65181), new OpaqueOptionValue(65181, intToByteArray(kid)));
		
	}
}
