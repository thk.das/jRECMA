package de.thk.rest.security.coap.recma.tvp;

import java.util.Date;

public class SimpleTimestampTvp implements TimeVariantParameter {
	
	private int delta;
	
	public SimpleTimestampTvp(){
		delta = 90000;
	}
	
	public SimpleTimestampTvp(int delta){
		this.delta = delta;
	}
	
	public int generate() {
		return (int) new Date().getTime();
	}

	public boolean verify(int tvp) {
		long now = new Date().getTime();
		long end = new Date(now+delta).getTime();
		long start = new Date(now-delta).getTime();
		if(tvp<=end  && tvp>=start ){
			return true;
		}
		
		else {
			return false;
		}
	}

}
