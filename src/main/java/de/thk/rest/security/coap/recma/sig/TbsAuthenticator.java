package de.thk.rest.security.coap.recma.sig;

public interface TbsAuthenticator {
	public byte[] sign(int kid, byte[] tbs) throws Exception;
	public boolean verify(int kid, byte[] tbs, byte[] sv);
	public String getName();
	public int getNumber();
}
