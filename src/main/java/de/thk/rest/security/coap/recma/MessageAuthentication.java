package de.thk.rest.security.coap.recma;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.thk.rest.security.coap.recma.hash.BodyHasher;
import de.thk.rest.security.coap.recma.sig.TbsAuthenticator;
import de.thk.rest.security.coap.recma.tvp.SimpleTimestampTvp;
import de.thk.rest.security.coap.recma.tvp.TimeVariantParameter;
import de.uzl.itm.ncoap.message.CoapMessage;
import de.uzl.itm.ncoap.message.options.OptionValue;

public class MessageAuthentication {
	private HashMap<Integer, TbsAuthenticator> tbsAuthenticatorsHashMap;
	private HashMap<Integer, BodyHasher> bodyHashersHashMap;
	private TimeVariantParameter timeVariantParameter;
	private ArrayList<Short> basicOptions;
	private ArrayList<Short> getOptions;
	private ArrayList<Short> bodyOptions;
	private ArrayList<Short> createdOptions;
	private ArrayList<Short> requestOptions;
	
	public MessageAuthentication(){
		this.bodyHashersHashMap = new HashMap<Integer, BodyHasher>();
		this.tbsAuthenticatorsHashMap = new HashMap<Integer, TbsAuthenticator>();
		this.timeVariantParameter = new SimpleTimestampTvp();
		basicOptions = new ArrayList<Short>();
		requestOptions = new ArrayList<Short>();
		getOptions = new ArrayList<Short>();
		bodyOptions = new ArrayList<Short>();
		createdOptions = new ArrayList<Short>();
		//Max-Age
		basicOptions.add((short)14);
		//Uri-*;
		requestOptions.add((short) 3);
		requestOptions.add( (short) 7);
		requestOptions.add((short) 11);
		requestOptions.add( (short) 15);
		//Accept
		getOptions.add((short) 17);
		//Content-Format and Payload-Length
		bodyOptions.add((short) 12);
		bodyOptions.add((short) 65001);
		//Location-Path and Location-Query
		createdOptions.add((short) 8);
		createdOptions.add((short) 20);
		
	}
	
	
	public void checkMessageSyntax(CoapMessage msg){
		//TODO 
	}
	
	public byte[] buildTbs(CoapMessage msg, int tvp ,int hash, List<Short> addOptions){
		
		checkMessageSyntax(msg);
		
		//tvp
		byte[] tbs = intToByteArray(tvp);
		//Version
		tbs = concatByteArrays(tbs, msg.getProtocolVersion());
		//Type
		int type = msg.getMessageType();
		tbs = concatByteArrays(tbs, type);
		//TokenLength
		tbs = concatByteArrays(tbs, msg.getToken().getBytes().length);
		//Code
		int code = msg.getMessageCode();
		tbs = concatByteArrays(tbs, code);
		//MessageID
		tbs = concatByteArrays(tbs, msg.getMessageID());
		if(code > 0){
			ArrayList<Short> tbsOptions = new ArrayList<Short>();
			tbsOptions.addAll(basicOptions);
			//GET
			if(code == 1){
				tbsOptions.addAll(getOptions);
			}
			//POST and PUT
			else if (code == 2 || code == 3){
				tbsOptions.addAll(bodyOptions);
			}
			//Request-Signature must consider Uri-* Options
			else if (code >= 1 || code <= 4){
				tbsOptions.addAll(requestOptions);
			}
			
			//Created
			else if(code == 65){
				tbsOptions.addAll(createdOptions);
			}
			
			//Check if response contains a payload
			else if(code >= 69 && msg.getContent().array().length > 0 ){
				tbsOptions.addAll(bodyOptions);
			}
			
			Collections.sort(tbsOptions);
			tbsOptions.addAll(addOptions);
			
			for(int optionNumber : msg.getAllOptions().keySet()){
	          
	            Iterator<OptionValue> iterator = msg.getOptions(optionNumber).iterator();
	            OptionValue optionValue = iterator.next();
	            tbs = concatByteArrays(tbs, optionValue.getValue());
	        }
			
			//TODO ResponseCodes
			BodyHasher bodyHasher = getBodyHashersHashMap().get(hash);
			
			byte[] hashOfBody = bodyHasher.hash(msg.getContent().array());
			if(msg.getContent().array().length > 0){
				hashOfBody = bodyHasher.hash(msg.getContent().array());
			} else {
				hashOfBody = bodyHasher.getHashOfEmptyBody();
			}
			
			tbs = concatByteArrays(tbs, hashOfBody);
		
		}
		
		return tbs;
	}
	
	public HashMap<Integer, TbsAuthenticator> getTbsAuthenticatorsHashMap() {
		return tbsAuthenticatorsHashMap;
	}


	public void setTbsAuthenticatorsHashMap(
			HashMap<Integer, TbsAuthenticator> tbsAuthenticatorsHashMap) {
		this.tbsAuthenticatorsHashMap = tbsAuthenticatorsHashMap;
	}


	public HashMap<Integer, BodyHasher> getBodyHashersHashMap() {
		return bodyHashersHashMap;
	}


	public void setBodyHashersHashMap(HashMap<Integer, BodyHasher> bodyHashersHashMap) {
		this.bodyHashersHashMap = bodyHashersHashMap;
	}


	public TimeVariantParameter getTimeVariantParameter() {
		return timeVariantParameter;
	}


	public void setTimeVariantParameter(TimeVariantParameter timeVariantParameter) {
		this.timeVariantParameter = timeVariantParameter;
	}


	public ArrayList<Short> getBasicOptions() {
		return basicOptions;
	}


	public void setBasicOptions(ArrayList<Short> basicOptions) {
		this.basicOptions = basicOptions;
	}


	public ArrayList<Short> getGetOptions() {
		return getOptions;
	}


	public void setGetOptions(ArrayList<Short> getOptions) {
		this.getOptions = getOptions;
	}


	public ArrayList<Short> getBodyOptions() {
		return bodyOptions;
	}


	public void setBodyOptions(ArrayList<Short> bodyOptions) {
		this.bodyOptions = bodyOptions;
	}

	
	public void addTbsAuthenticator(TbsAuthenticator tbsAuthenticator){
		tbsAuthenticatorsHashMap.put(tbsAuthenticator.getNumber(), tbsAuthenticator);
	}
	
	public void addTbsAuthenticators(List<TbsAuthenticator> tbsAuthenticators) {
		for (TbsAuthenticator tbsAuthenticator : tbsAuthenticators) {
			tbsAuthenticatorsHashMap.put(tbsAuthenticator.getNumber(), tbsAuthenticator);
		}
	}

	public void addBodyHasher(BodyHasher bodyHasher){
		bodyHashersHashMap.put(bodyHasher.getNumber(), bodyHasher);
	}
	
	public void addBodyHashers(List<BodyHasher> bodyHashers) {
		for(BodyHasher bodyHasher: bodyHashers){
			bodyHashersHashMap.put(bodyHasher.getNumber(), bodyHasher);
		}
	}

	protected byte[] intToByteArray(int number){
		return ByteBuffer.allocate(4).putInt(number).array();
	}
	
	protected byte[] shortToByteArray(short number){
		return ByteBuffer.allocate(2).putShort(number).array();
	}
	
	protected int byteArrayToInt(byte[] number){
		return ByteBuffer.wrap(number).getInt();
	}
	
	protected short byteArrayToShort(byte[] number){
		return ByteBuffer.wrap(number).getShort();
	}
	
	protected byte[] concatByteArrays(byte[] a, byte[] b){
		byte[] c = new byte[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		return c;
	}
	
	protected byte[] concatByteArrays(byte[] a, int b){
		byte[] bAsByteArray = intToByteArray(b);
		return concatByteArrays(a, bAsByteArray);
	}
	
}
