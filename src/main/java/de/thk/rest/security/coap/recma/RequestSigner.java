package de.thk.rest.security.coap.recma;

import java.util.ArrayList;
import java.util.List;

import de.thk.rest.security.coap.recma.hash.BodyHasher;
import de.thk.rest.security.coap.recma.sig.TbsAuthenticator;
import de.uzl.itm.ncoap.message.CoapRequest;



public class RequestSigner extends MessageSigner {
	
	private int kid;
	private int hash;
	private int sig;
	private ArrayList<Short> addOptions;
	
	
	public RequestSigner(short kid, TbsAuthenticator tbsSigner, BodyHasher bodyHasher){
		this();
		this.kid = kid;
		this.sig = tbsSigner.getNumber();
		this.hash = bodyHasher.getNumber();
		this.addOptions = new ArrayList<>();
		addTbsAuthenticator(tbsSigner);
		addBodyHasher(bodyHasher);
	}
	
	protected RequestSigner() {
		super();
	}
	
	protected RequestSigner(List<TbsAuthenticator> tbsAuthenticators, List<BodyHasher> bodyHashers){
		this();
		addTbsAuthenticators(tbsAuthenticators);
		addBodyHashers(bodyHashers);
	}
	
	public void sign(CoapRequest req){
		try {
			super.sign(req, kid, sig, hash, addOptions);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
