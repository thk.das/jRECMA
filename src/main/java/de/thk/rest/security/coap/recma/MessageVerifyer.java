package de.thk.rest.security.coap.recma;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.thk.rest.security.coap.recma.hash.BodyHasher;
import de.thk.rest.security.coap.recma.sig.TbsAuthenticator;
import de.uzl.itm.ncoap.message.CoapMessage;
import de.uzl.itm.ncoap.message.options.OptionValue;


public class MessageVerifyer extends MessageAuthentication{
	
	public MessageVerifyer(TbsAuthenticator tbsAuthenticator, BodyHasher bodyHasher){
		this.getTbsAuthenticatorsHashMap().put(tbsAuthenticator.getNumber(), tbsAuthenticator);
		this.getBodyHashersHashMap().put(bodyHasher.getNumber(), bodyHasher);
	}
	
	public boolean verify(CoapMessage msg){
		int tvp = byteArrayToInt(msg.getAllOptions().get(65053).iterator().next().getValue());
		short hash = byteArrayToShort(msg.getAllOptions().get(65053).iterator().next().getValue());
		short sig = byteArrayToShort(msg.getAllOptions().get(65053).iterator().next().getValue());
		short kid = byteArrayToShort(msg.getAllOptions().get(65181).iterator().next().getValue());
		byte[] sv = msg.getOptions(65117).iterator().next().getValue();
		List<Short> addOptions = new ArrayList<Short>();
		
		for(Iterator<OptionValue> iterator = msg.getAllOptions().get(65149).iterator(); iterator.hasNext();){
			addOptions.add(byteArrayToShort(iterator.next().getValue()));
		}
		
		byte[] tbs = buildTbs(msg, tvp, hash, addOptions);
		TbsAuthenticator tbsAuthenticator = getTbsAuthenticatorsHashMap().get(sig);
		return tbsAuthenticator.verify(kid, tbs, sv);
	}
}
