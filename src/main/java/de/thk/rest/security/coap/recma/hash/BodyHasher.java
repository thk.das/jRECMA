package de.thk.rest.security.coap.recma.hash;

public interface BodyHasher {
	public byte[] hash(byte[] body);
	public String getName();
	public int getNumber();
	public byte[] getHashOfEmptyBody();
}
