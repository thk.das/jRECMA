/*******************************************************************************
 * Copyright 2015 Hoai Viet Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.thk.rest.security.coap.recma.sig;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class HmacSha256Authenticator implements TbsAuthenticator {
	
	private HashMap<Integer,byte[]> hmacKeyStore;
	
	public HmacSha256Authenticator() {
		hmacKeyStore = new HashMap<Integer, byte[]>();
	}
	
	public HmacSha256Authenticator(HashMap<Integer,byte[]> hmacKeyStore){
		this.hmacKeyStore = hmacKeyStore;
	}
	
	public HashMap<Integer, byte[]> getHmacKeyStore() {
		return hmacKeyStore;
	}

	public void setHmacKeyStore(HashMap<Integer, byte[]> hmacKeyStore) {
		this.hmacKeyStore = hmacKeyStore;
	}

	@Override
	public byte[] sign(int kid, byte[] tbs) throws Exception {
		if(hmacKeyStore.size() == 0){
			throw new Exception();
		}
		
		byte[] key = hmacKeyStore.get(kid);
		if(key == null){
			throw new NullPointerException("Unkown Key id");
		}
		Mac sha256_HMAC = null;
		try {
			sha256_HMAC = Mac.getInstance("HmacSHA256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		  SecretKeySpec secret_key = new SecretKeySpec(key, "HmacSHA256");
		  try {
			sha256_HMAC.init(secret_key);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		  return sha256_HMAC.doFinal(tbs);
	}

	@Override
	public boolean verify(int kid, byte[] tbs, byte[] sv) {
		Mac sha256_HMAC = null;
		byte[] key = hmacKeyStore.get(kid);
		if(key == null){
//			throw new NotAuthenticatedExpection("Unknown Key id");
		}
		try {
			sha256_HMAC = Mac.getInstance("HmacSHA256");
		} catch (NoSuchAlgorithmException e) {
			
			return false;
		}
		  SecretKeySpec secret_key = new SecretKeySpec(key, "HmacSHA256");
		  try {
			sha256_HMAC.init(secret_key);
		} catch (InvalidKeyException e) {
			return false;
		}
		
		try {
			byte[] svTemp = sha256_HMAC.doFinal(tbs);
			for (int i = 0; i < svTemp.length; i++) {
				if(svTemp[i] != sv[i]){
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			
			return false;
		}
		
		
	}

	@Override
	public String getName() {
		return "HMAC/SHA256";
	}

	@Override
	public int getNumber() {
		// TODO Auto-generated method stub
		return 1;
	}

}
