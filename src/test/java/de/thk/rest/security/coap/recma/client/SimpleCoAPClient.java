package de.thk.rest.security.coap.recma.client;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.HashMap;

import de.thk.rest.security.coap.recma.RequestSigner;
import de.thk.rest.security.coap.recma.hash.Sha256Hasher;
import de.thk.rest.security.coap.recma.sig.HmacSha256Authenticator;
import de.uzl.itm.ncoap.application.client.ClientCallback;
import de.uzl.itm.ncoap.application.client.CoapClient;
import de.uzl.itm.ncoap.message.CoapRequest;
import de.uzl.itm.ncoap.message.CoapResponse;
import de.uzl.itm.ncoap.message.options.Option;
import de.uzl.itm.ncoap.message.options.StringOptionValue;


public class SimpleCoAPClient {
	public static void main(String[] args) throws IllegalArgumentException, URISyntaxException, IOException {
		
		String base64Key = "";
		BufferedReader br = new BufferedReader(new FileReader("key.txt"));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        base64Key = sb.toString();
	    } finally {
	        br.close();
	    }
	    
	    byte[] key =  Base64.getMimeDecoder().decode(base64Key);
	   
		HashMap<Integer, byte[]> hmacKeyStore = new HashMap<>();
		
		hmacKeyStore.put(1, key);
		
		HmacSha256Authenticator hmacSha256Authenticator = new HmacSha256Authenticator(hmacKeyStore);
		
		Sha256Hasher sha256Hasher = new Sha256Hasher();
		
		RequestSigner requestSigner = new RequestSigner((short)1,hmacSha256Authenticator, sha256Hasher);
		CoapClient client = new CoapClient();
	
		CoapRequest req = new CoapRequest(0, 1);
		
		int messageID = new SecureRandom().nextInt(1000);
	 
		req.setMessageID(messageID);
		
		requestSigner.sign(req);
		
		client.sendCoapRequest(req, new InetSocketAddress("coap.me", 5683),new ClientCallback() {
			
			@Override
			public void processCoapResponse(CoapResponse coapResponse) {
				// TODO Auto-generated method stub
				System.out.println(coapResponse);
				
			}
		});

		
		System.out.println(req);
	}
}
