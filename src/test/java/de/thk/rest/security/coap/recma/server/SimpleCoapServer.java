/**
 * Copyright (c) 2012, Oliver Kleine, Institute of Telematics, University of Luebeck
 * All rights reserved
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *  - Redistributions of source messageCode must retain the above copyright notice, this list of conditions and the following
 *    disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  - Neither the name of the University of Luebeck nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.thk.rest.security.coap.recma.server;

import de.thk.rest.security.coap.recma.MessageVerifyer;
import de.thk.rest.security.coap.recma.RequestVerifyer;
import de.thk.rest.security.coap.recma.hash.BodyHasher;
import de.thk.rest.security.coap.recma.hash.Sha256Hasher;
import de.thk.rest.security.coap.recma.sig.HmacSha256Authenticator;
import de.thk.rest.security.coap.recma.sig.TbsAuthenticator;
import de.uzl.itm.ncoap.application.server.CoapServer;
import de.uzl.itm.ncoap.communication.blockwise.BlockSize;
import de.uzl.itm.ncoap.message.options.OptionValue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Base64;
import java.util.HashMap;
import java.util.concurrent.ScheduledExecutorService;

/**
 * This is a simple application to showcase how to use nCoAP for servers
 *
 * @author Oliver Kleine
 */
public class SimpleCoapServer extends CoapServer {
	
	private MessageVerifyer messageVerifyer;

    private void registerSimpleNotObservableWebresource() {
        // create initial status (one string per paragraph)
        String[] status = new String[10];
        for (int i = 0; i < status.length; i++) {
            status[i] = "This is paragraph #" + (i + 1);
        }

        // register resource at server
        this.registerWebresource(new SimpleNotObservableWebresource(
                "/simple", status, OptionValue.MAX_AGE_MAX, this.getExecutor()
        ));
    }

    private void registerSimpleObservableTimeResource() {
        // register resource at server
        this.registerWebresource(new SimpleObservableTimeService("/utc-time", 5, this.getExecutor()));
    }
    
    

    /**
     * Creates a new instance of {@link SimpleCoapServer}
     * @param block1Size the preferred, i.e. max. {@link BlockSize} for requests
     * @param block2Size the preferred, i.e. max {@link BlockSize} for responses
     */
    public SimpleCoapServer(BlockSize block1Size, BlockSize block2Size) {
        super(block1Size, block2Size);
    }
    
    /**
     * Creates a new instance of {@link SimpleCoapServer}
     * @param block1Size the preferred, i.e. max. {@link BlockSize} for requests
     * @param block2Size the preferred, i.e. max {@link BlockSize} for responses
     */
    public SimpleCoapServer(BlockSize block1Size, BlockSize block2Size, MessageVerifyer messageVerifyer) {
        super(block1Size, block2Size);
        this.messageVerifyer = messageVerifyer;
    }

    /**
     * Starts a {@link CoapServer} instance with two {@link de.uzl.itm.ncoap.application.server.resource.Webresource}
     * instances where one is observable and the other one is not.
     *
     * @param args no arguments needed (may be empty)
     * @throws Exception if some unexpected error occurred
     */
    public static void main(String[] args) throws Exception {
        // configure logging
       // LoggingConfiguration.configureDefaultLogging();
        String base64Key = "";
		BufferedReader br = new BufferedReader(new FileReader("key.txt"));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        base64Key = sb.toString();
	    } finally {
	        br.close();
	    }
	    byte[] key =  Base64.getMimeDecoder().decode(base64Key);
	   
		HashMap<Integer, byte[]> hmacKeyStore = new HashMap<>();
		
		hmacKeyStore.put(1, key);
		
		HmacSha256Authenticator hmacSha256Authenticator = new HmacSha256Authenticator(hmacKeyStore);
		
        Sha256Hasher sha256Hasher  = new Sha256Hasher();
        
        MessageVerifyer messageVerifyer = new MessageVerifyer(hmacSha256Authenticator, sha256Hasher);

        // create server and register resources
        SimpleCoapServer server = new SimpleCoapServer(BlockSize.SIZE_16, BlockSize.SIZE_16, messageVerifyer);
        server.registerSimpleNotObservableWebresource();
        server.registerSimpleObservableTimeResource();
    }
}
