# Java-based REST-ful CoAP Message Authentication (jRECMA)
This is the jRECMA project, a Java library for signing and verifying REST-ful CoAP Messages.
jRECMA is an implementation of the REST-ful Message Authentication (REMA) scheme based on the paper "Authentication Scheme for REST" [1].

More tutorials and features will be published soon. 

## Version 
0.0.1 Dev

## Downloads
* JAR containing the binaries: http://das.th-koeln.de/REMA/RECMA/jRECMA/jRECMA-v.0.0.1-dev.jar
* JAR containing the sources: http://das.th-koeln.de/REMA/RECMA/jRECMA/jRECMA-v.0.0.1-dev-sources.jar

## Dependencies
* [Apache Commons IO 2.4](https://commons.apache.org/proper/commons-io/download_io.cgi)
* [Junit 4.12](https://github.com/junit-team/junit/wiki/Download-and-Install)

## Requirements
Java 1.7 or higher

# Getting Started
Please vist the Wiki for further information on working with jRECMA:

[Wiki](https://gitlab.com/thk.das/jRECMA/wikis/home)

# References
[1] Luigi Lo Iacono and Hoai Viet Nguyen: "Authentication Scheme for REST", in International Conference on Future Network Systems and Security (FNSS), 2015.
[Online]. Available at: [http://link.springer.com/chapter/10.1007/978-3-319-19210-9_8](http://link.springer.com/chapter/10.1007/978-3-319-19210-9_8)

[2] Hoai Viet Nguyen and Luigi Lo Iacono: "REST-ful CoAP Message Authentication", in International Workshop on Secure Internet of Things (SIoT), in conjunction with the European Symposium on Research in Computer Security (ESORICS), 2015.
[Online]. Available at: [https://dx.doi.org/10.1109/SIOT.2015.8](https://dx.doi.org/10.1109/SIOT.2015.8)

[3] Hoai Viet Nguyen and Luigi Lo Iacono: "RESTful IoT Authentication Protocols", in Mobile Security and Privacy - Advances, Challenges and Future Research Directions, Elsevier/Syngress, 2016.
[Online]. Available at: [http://dx.doi.org/10.1016/B978-0-12-804629-6.00010-9](http://dx.doi.org/10.1016/B978-0-12-804629-6.00010-9)
